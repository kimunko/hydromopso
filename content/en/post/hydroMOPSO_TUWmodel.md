---
date: 2023-04-24
description: ""
featured_image: "/TUWmodel_image.png"
tags:
title: "Tutorial for using hydroMOPSO to calibrate TUWmodel"
---

The main objective of this tutorial is to show how to use `hydroMOPSO` to find an a set of solutions that satisfy Pareto optimality in the multi-objective calibration of `TUWmodel` ((Parajka et al., 2007) <doi:10.1002/HYP.6253>). 


{{< rawhtml >}}

<a href="https://kimunko.gitlab.io/hydromopso/hydroMOPSO_TUWmodel_vignette.pdf" target="_blank">Open tutorial</a>

{{< /rawhtml >}}
