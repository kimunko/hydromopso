---
title: "hydroMOPSO"

description: "Multi-Objective Calibration of Hydrological Models using MOPSO"
bibliography: TUWmodel_hydroMOPSO.bib
cascade:
  featured_image: '/fondo_hydroMOPSO.png'
---


{{< rawhtml >}}

<div style="text-align: justify"> 

State-of-the-art Multi-Objective Particle Swarm Optimiser (MOPSO), based on the algorithm developed by Lin et al. (2018) with improvements described by Marinao-Rivas & Zambrano-Bigiarini (2020). This package is inspired by and closely follows the philosophy of the single objective 'hydroPSO' R package (Zambrano-Bigiarini & Rojas, 2013), and can be used for global optimisation of non-smooth and non-linear R functions and R-base models (e.g., 'TUWmodel', 'GR4J', 'GR6J'). However, the main focus of 'hydroMOPSO' is optimising environmental and other real-world models that need to be run from the system console (e.g., 'SWAT+'). 'hydroMOPSO' communicates with the model to be optimised through its input and output files, without requiring modifying its source code. Thanks to its flexible design and the availability of several fine-tuning options, 'hydroMOPSO' can tackle a wide range of multi-objective optimisation problems (e.g., multi-objective functions, multiple model variables, multiple periods). Finally, 'hydroMOPSO' is designed to run on multi-core machines or network clusters, to alleviate the computational burden of complex models with long execution time.

</div>

{{< /rawhtml >}}


## Installing hydroMOPSO in R


You can install hydroMOPSO from CRAN:


```r
install.packages("hydroMOPSO")

```


## Key features


{{< rawhtml >}}

<div style="text-align: justify"> 

<ul>
 <li>Very competitive state-of-the-art multi-objective optimisation algorithm NMPSO.</li>
 <li>Several fine tuning options, with recommended default settings.</li>
 <li>Automated plotting of high quality graphics to support decision making.</li>
 <li>Open-source and multi-platform (Windows, GNU/Linux, macOS).</li>
 <li>Model-independent and parallel-capable.</li>
</ul>

<br><br>

</div>

<div style="text-align: center"> 


{{< /rawhtml >}}