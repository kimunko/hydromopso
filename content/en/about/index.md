---
title: "About"
description: ""
featured_image: ''
menu:
  main:
    weight: 30
---

{{< rawhtml >}}


<div style="text-align: center"> 

<h1>Developers</h1>

Ing. Rodrigo Marinao-Rivas<sup>1,2</sup><br>
<a href="mailto:r.marinao01@ufromail.com">r.marinao01@ufromail.cl</a><br>
<br>
<br>
Dr. Mauricio Zambrano-Bigiarini<sup>1,2</sup><br>
<a href="mauricio.zambrano@ufrontera.cl">mauricio.zambrano@ufrontera.cl</a><br>
<a href="https://hzambran.github.io/">https://hzambran.github.io/</a><br>
<br>
<br>
<sup>1</sup>Department of Civil Engineering, Universidad de La Frontera, Temuco, Chile<br>
<sup>2</sup>Center for Climate and Resilience Research, Universidad de Chile, Santiago, Chile<br>
<br>
<br>


<a href="https://ingenieriacivil.ufro.cl/" target="_blank"><img src="/hydromopso/logo_ufro.png"></a>
<a href="https://cr2.cl/" target="_blank"><img src="/hydromopso/logo_cr2.png"></a>

</div>

{{< /rawhtml >}}




